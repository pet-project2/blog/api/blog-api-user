﻿using Application.Query.GetTokenAccess;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Api.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class MeController : ControllerBase
    {
        private readonly IMediator _mediator;
        public MeController(IMediator mediator)
        {
            this._mediator = mediator;
        }

        [AllowAnonymous]
        [HttpPost("token")]
        public async Task<IActionResult> GetTokenAcessAsync(GetAccessTokenQuery query)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest("Username or Password is invalid");

                var accessToken = await this._mediator.Send(query);
                return Ok(accessToken);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpPost("profile")]
        public async Task<IActionResult> GetUserProfileAsync()
        {
            try
            {
                var principal = this.User;
                if (null != principal)
                {
                    foreach (Claim claim in principal.Claims)
                    {
                        Debug.WriteLine("CLAIM TYPE: " + claim.Type + "; CLAIM VALUE: " + claim.Value + "</br>");
                    }

                }

                return Ok();
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }
    }
}
