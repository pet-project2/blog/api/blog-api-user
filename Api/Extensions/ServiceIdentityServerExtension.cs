﻿using Infrastructure.IntegrationAppConfig;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Extensions
{
    public static class ServiceIdentityServerExtension
    {
        public static void AddIdentityServer(this IServiceCollection services, IConfiguration configuration)
        {
            var identityServerConfig = configuration.GetSection("IdentityServer");
            var identityServerSettings = identityServerConfig.Get<IdentityServerConfig>();

            services.Configure<IdentityServerConfig>(identityServerConfig);

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                    .AddJwtBearer(JwtBearerDefaults.AuthenticationScheme, options =>
                    {
                        options.Authority = identityServerSettings.Uri;
                        options.TokenValidationParameters = new TokenValidationParameters
                        {
                            ValidateAudience = false
                        };
                    });
        }
    }
}
