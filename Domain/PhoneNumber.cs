﻿using Fury.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class PhoneNumber : ValueObject
    {
        private string _value { get; }

        public static implicit operator string(PhoneNumber phoneNumber) => phoneNumber._value;

        internal PhoneNumber(string phoneNumber) => this._value = phoneNumber;

        protected override IEnumerable<object> GetEqualityComponents()
        {
            yield return _value;
        }

        public static PhoneNumber FromString(string phoneNumber) => new PhoneNumber(phoneNumber);

        public override string ToString()
        {
            return this._value;
        }
    }
}
