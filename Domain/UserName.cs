﻿using Fury.Core.Models;
using System.Collections.Generic;

namespace Domain
{
    public class UserName : ValueObject
    {
        private string _value { get; }

        public static implicit operator string(UserName userName) => userName._value;

        internal UserName(string userName) => this._value = userName;

        protected override IEnumerable<object> GetEqualityComponents()
        {
            yield return _value;
        }

        public static UserName FromString(string userName) => new UserName(userName);

        public override string ToString()
        {
            return this._value;
        }
    }
}
