﻿using Fury.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class UserFirstName : ValueObject
    {
        private string _value { get; }

        public static implicit operator string(UserFirstName userFirstName) => userFirstName._value;

        internal UserFirstName(string userFirstName) => this._value = userFirstName;

        protected override IEnumerable<object> GetEqualityComponents()
        {
            yield return _value;
        }

        public static UserFirstName FromString(string userFirstName) => new UserFirstName(userFirstName);

        public override string ToString()
        {
            return this._value;
        }
    }
}
