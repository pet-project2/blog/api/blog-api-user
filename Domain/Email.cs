﻿using Fury.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class Email : ValueObject
    {
        private string _value { get; }

        public static implicit operator string(Email email) => email._value;

        internal Email(string email) => this._value = email;

        protected override IEnumerable<object> GetEqualityComponents()
        {
            yield return _value;
        }

        public static Email FromString(string email) => new Email(email);

        public override string ToString()
        {
            return this._value;
        }
    }
}
