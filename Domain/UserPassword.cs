﻿using Fury.Core.Models;
using System.Collections.Generic;

namespace Domain
{
    public class UserPassword : ValueObject
    {
        private string _value { get; }

        public static implicit operator string(UserPassword userPassword) => userPassword._value;

        internal UserPassword(string userPassword) => this._value = userPassword;

        protected override IEnumerable<object> GetEqualityComponents()
        {
            yield return _value;
        }

        public static UserPassword FromString(string userPassword) => new UserPassword(userPassword);

        public override string ToString()
        {
            return this._value;
        }
    }
}
