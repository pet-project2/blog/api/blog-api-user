﻿using Fury.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class UserLastName : ValueObject
    {
        private string _value { get; }

        public static implicit operator string(UserLastName userLastName) => userLastName._value;

        internal UserLastName(string userLastName) => this._value = userLastName;

        protected override IEnumerable<object> GetEqualityComponents()
        {
            yield return _value;
        }

        public static UserLastName FromString(string userLastName) => new UserLastName(userLastName);

        public override string ToString()
        {
            return this._value;
        }
    }
}
