﻿using Fury.Core.Interface;
using Fury.Core.Models;
using System;

namespace Domain.Entities
{
    public class UserAggregate : Entity, IAggregateRoot
    {
        public UserName UserName { get; private set; }
        public UserPassword Password { get; private set; }
        public UserFirstName FirstName { get; private set; }
        public UserLastName LastName { get; private set; }
        public Email Email { get; private set; }
        public Email NormalizedEmail { get; private set; }
        public UserName NormalizedUserName { get; set; }
        public PhoneNumber PhoneNumber { get; private set; }
        public UserAggregate(Guid id) : base(id) { }

        public UserAggregate(Guid id, string firstName, string lastName, string email, string phoneNumber) : this(id)
        {
            this.UserName = UserName.FromString(email);
            this.NormalizedUserName = UserName.FromString(email);
            this.FirstName = UserFirstName.FromString(firstName);
            this.LastName = UserLastName.FromString(lastName);
            this.Email = Email.FromString(email);
            this.NormalizedEmail = Email.FromString(email);
            this.PhoneNumber = PhoneNumber.FromString(phoneNumber);
        }

        public void UpdatePassword(string password)
        {
            this.Password = UserPassword.FromString(password);
        }

        public void UpdateFirstName(string firstName)
        {
            this.FirstName = UserFirstName.FromString(firstName);
        }

        public void UpdateLastName(string lastName)
        {
            this.LastName = UserLastName.FromString(lastName);
        }

        public void UpdateEmail(string email)
        {
            this.Email = Email.FromString(email);
            this.NormalizedEmail = Email.FromString(email);
        }

        public void UpdatePhoneNumber(string phoneNumber)
        {
            this.PhoneNumber = PhoneNumber.FromString(phoneNumber);
        }
    }
}
