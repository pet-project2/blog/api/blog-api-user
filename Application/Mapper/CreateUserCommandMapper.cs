﻿using Application.Command.Create;
using AutoMapper;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Mapper
{
    public class CreateUserCommandMapper : Profile
    {
        public CreateUserCommandMapper()
        {
            CreateMap<CreateUserCommand, UserAggregate>()
                .ConstructUsing(x => new UserAggregate(Guid.NewGuid(), x.FirstName, x.LastName, x.Email, x.PhoneNumber))
                .ForAllMembers(s => s.Ignore());
        }
    }
}
