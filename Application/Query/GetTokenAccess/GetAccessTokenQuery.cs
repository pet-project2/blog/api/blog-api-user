﻿using MediatR;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Query.GetTokenAccess
{
    public class GetAccessTokenQuery : IRequest<string>
    {
        [Required(ErrorMessage = "Username must not be null or empty")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Username must not be null or empty")]
        public string Password { get; set; }
    }
}
