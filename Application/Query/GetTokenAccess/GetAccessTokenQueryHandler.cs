﻿using Infrastructure.Service;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Query.GetTokenAccess
{
    public class GetAccessTokenQueryHandler : IRequestHandler<GetAccessTokenQuery, string>
    {
        private readonly IIdentityService _identityService;
        public GetAccessTokenQueryHandler(IIdentityService identityService)
        {
            this._identityService = identityService;
        }
        public async Task<string> Handle(GetAccessTokenQuery request, CancellationToken cancellationToken)
        {
            var username = request.UserName;
            var password = request.Password;

            var accessToken = await this._identityService.GetTokenAccessAsync(username, password);

            return accessToken;
        }
    }
}
