﻿using MediatR;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Command.Create
{
    public class CreateUserCommand : IRequest
    {
        [Required(ErrorMessage = "First Name must not be null or empty")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Last Name must not be null or empty")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Email must not be null or empty")]
        [DataType(DataType.EmailAddress)]
        [EmailAddress(ErrorMessage = "Invalid email address")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Password must not be null or empty")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required(ErrorMessage = "Password Confirm must not be null or empty")]
        [DataType(DataType.Password)]
        [Compare(nameof(Password), ErrorMessage = "Password not match")]
        public string PasswordConfirm { get; set; }

        public string PhoneNumber { get; set; }
    }
}
