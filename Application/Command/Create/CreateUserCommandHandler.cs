﻿using Application.Exceptions;
using Application.Mapper;
using Domain.Entities;
using Infrastructure.Entity;
using Infrastructure.Mapper;
using Infrastructure.Respository;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Command.Create
{
    public class CreateUserCommandHandler : IRequestHandler<CreateUserCommand, Unit>
    {
        private readonly IUserRepository<User> _userRepository;
        public CreateUserCommandHandler(IUserRepository<User> userRepository)
        {
            this._userRepository = userRepository;
        }
        public async Task<Unit> Handle(CreateUserCommand request, CancellationToken cancellationToken)
        {
            var userAggregate = ApplicationMapping.Mapper.Map<UserAggregate>(request);

            var user = EntityMapping.Mapper.Map<UserAggregate, User>(userAggregate);
            await this._userRepository.CreateUserAsync(user, request.Password);

            return await Task.FromResult(Unit.Value);
        }
    }
}
