﻿using IdentityModel.Client;
using Infrastructure.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Service
{
    public class IdentityService : IIdentityService
    {
        private readonly HttpClient _httpClient;
        public IdentityService(HttpClient httpClient)
        {
            httpClient.BaseAddress = new Uri("https://localhost:9001");
            this._httpClient = httpClient;
        }

        public async Task<string> GetTokenAccessAsync(string username, string password)
        {
            var disco = await _httpClient.GetDiscoveryDocumentAsync();
            if (disco.IsError)
                throw new IdentityServiceException(disco.Error);

            var tokenResponse = await this._httpClient.RequestPasswordTokenAsync(new PasswordTokenRequest
            {
                Address = disco.TokenEndpoint,
                ClientId = "blog.client.credential",
                ClientSecret = "y_K+j.[F9zYa6#E4",
                Scope = "api",
                UserName = username,
                Password = password
            });

            if (tokenResponse.IsError)
                throw new IdentityServiceException(tokenResponse.Error);

            return tokenResponse.AccessToken;
        }
    }
}
