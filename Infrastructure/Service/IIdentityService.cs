﻿using System.Threading.Tasks;

namespace Infrastructure.Service
{
    public interface IIdentityService
    {
        Task<string> GetTokenAccessAsync(string username, string password);
    }
}
