﻿using Ardalis.Specification;
using Ardalis.Specification.EntityFrameworkCore;
using Fury.Core.Interface;
using Infrastructure.Entity;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Infrastructure.Respository
{
    public class UserRepository<T> : RepositoryBase<T>, IUserRepository<T> where T : class, IAggregateRoot
    {
        private readonly UserManager<T> _userManager;
        private readonly AppDbContext _dbContext;
        public UserRepository(AppDbContext dbContext, UserManager<T> userManager) : base(dbContext)
        {
            this._dbContext = dbContext;
            this._userManager = userManager;
        }

        public Task CreateUserAsync(T user, string password)
        {
             return _userManager.CreateAsync(user, password);
        }

        public Task UpdateUserAsync(T user)
        {
            return _userManager.UpdateAsync(user);
        }
    }
}
