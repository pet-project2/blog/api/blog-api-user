﻿using Ardalis.Specification.EntityFrameworkCore;
using Fury.Core.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Respository
{
    public class EfRepository<T> : RepositoryBase<T>, IRepository<T> where T : class, IAggregateRoot
    {
        private readonly AppDbContext _dbContext;
        public EfRepository(AppDbContext dbContext) : base(dbContext)
        {
            this._dbContext = dbContext;
        }
    }
}
