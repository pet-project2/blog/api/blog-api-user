﻿using Fury.Core.Interface;
using Infrastructure.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Respository
{
    public interface IUserRepository<T> : IRepository<T> where T : class, IAggregateRoot
    {
        Task CreateUserAsync(T user, string password);
        Task UpdateUserAsync(T user);
    }
}
