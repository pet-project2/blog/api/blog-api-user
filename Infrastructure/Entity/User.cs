﻿using Fury.Core.Interface;
using Microsoft.AspNetCore.Identity;
using System;

namespace Infrastructure.Entity
{
    public class User : IdentityUser<Guid>, IAggregateRoot
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime CreatedDateTime { get; set; } = DateTime.Now.ToUniversalTime();
    }
}
