﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Mapper
{
    public class EntityMapping
    {
        public static IMapper Mapper => _lazy.Value;

        private static readonly Lazy<IMapper> _lazy = new(() =>
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<UserMapper>();
            });
            var mapper = config.CreateMapper();
            return mapper;
        });
    }
}
