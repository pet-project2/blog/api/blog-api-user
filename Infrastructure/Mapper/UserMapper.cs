﻿using AutoMapper;
using Domain.Entities;
using Infrastructure.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Mapper
{
    public class UserMapper : Profile
    {
        public UserMapper()
        {
            //CreateMap<User, UserAggregate>()
            //    .ConstructUsing(x => new UserAggregate(x.Id, x.FirstName, x.LastName, x.Email, x.PhoneNumber))
            //    .ForAllMembers(s => s.Ignore());

            CreateMap<UserAggregate, User>()
                .ForMember(d => d.UserName, s => s.MapFrom(src => src.UserName))
                .ForMember(d => d.NormalizedUserName, s => s.MapFrom(src => src.NormalizedUserName))
                .ForMember(d => d.FirstName, s => s.MapFrom(src => src.FirstName))
                .ForMember(d => d.LastName, s => s.MapFrom(src => src.LastName))
                .ForMember(d => d.Email, s => s.MapFrom(src => src.Email))
                .ForMember(d => d.NormalizedEmail, s => s.MapFrom(src => src.NormalizedEmail))
                .ForMember(d => d.PhoneNumber, s => s.MapFrom(src => src.PhoneNumber));
        }
    }
}
